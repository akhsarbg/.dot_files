# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=
export PATH="~/bin:$PATH"
export PATH="~/bin/ccache:$PATH"
export PATH="~/bin/emacs25/bin:$PATH"
export PATH="~/bin/ccache:$PATH"


# User specific aliases and functions
alias cadence="ssh -Yt rh6login.ifi.uio.no 'cd bin/cadence; source CRN90LP_session_IC616; virtuoso'"
alias fuck='eval $(thefuck $(fc -ln -1)); history -r'
alias reset='tput reset'
alias ls="ls --group-directories-first --color=auto"
alias ll='ls -lh'
alias dmesg='dmesg -L'
alias open='xdg-open'

LS_COLORS="$LS_COLORS:di=00;33"    # Hard links highlighted
#LS_COLORS="$LS_COLORS:di=00;33:mh=0"    # Hard links default color

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '

man() {
    env LESS_TERMCAP_mb=$'\E[01;31m' \
	LESS_TERMCAP_md=$'\E[01;38;5;74m' \
	LESS_TERMCAP_me=$'\E[0m' \
	LESS_TERMCAP_se=$'\E[0m' \
	LESS_TERMCAP_so=$'\E[38;5;246m' \
	LESS_TERMCAP_ue=$'\E[0m' \
	LESS_TERMCAP_us=$'\E[04;38;5;146m' \
	man "$@"
}
