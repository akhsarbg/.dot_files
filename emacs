;; we can require features
(require 'cl)
(require 'package)

;; add mirrors for list-packages
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

;; needed to use things downloaded with the package manager
(package-initialize)

;; install some packages if missing
(let* ((packages '(auto-complete
                   latex-preview-pane
                   ido-vertical-mode
                   monokai-theme
                   multiple-cursors
                   undo-tree
                   ;; if you want more packages, add them here
                   ))
       (packages (remove-if 'package-installed-p packages)))
  (when packages
    (package-refresh-contents)
    (mapc 'package-install packages)))

;; no splash screen
(setq inhibit-splash-screen t)

;; show matching parenthesis
(show-paren-mode 1)

;; show column number in mode-line
(column-number-mode 1)

;; overwrite marked text
(delete-selection-mode 1)

;; enable ido-mode, changes the way files are selected in the minibuffer
(ido-mode 1)

;; use ido everywhere
(ido-everywhere 1)

;; show vertically
(ido-vertical-mode 1)

;; use undo-tree-mode globally
(global-undo-tree-mode 1)

;; stop blinking cursor
(blink-cursor-mode 0)

;; no menubar
(menu-bar-mode 0)

;; no toolbar
(tool-bar-mode 0)

;; no scrollbar
(scroll-bar-mode 0)

;; global-linum-mode shows line numbers in all buffers, exchange 0
;; with 1 to enable this feature
(global-linum-mode 0)

;; answer with y/n
(fset 'yes-or-no-p 'y-or-n-p)

;; choose a color-theme
(load-theme 'monokai t)

;; Change command color to fiolet
(set-face-foreground 'font-lock-comment-face "violet")

;; get the default config for auto-complete (downloaded with
;; package-manager)
(require 'auto-complete-config)

;; load the default config of auto-complete
(ac-config-default)

;; kills the active buffer, not asking what buffer to kill.
(global-set-key (kbd "C-x k") 'kill-this-buffer)

;; adds all autosave-files (i.e #test.txt#, test.txt~) in one
;; directory, avoid clutter in filesystem.
(defvar emacs-autosave-directory (concat user-emacs-directory "autosaves/"))
(setq backup-directory-alist
      `((".*" . ,emacs-autosave-directory))
      auto-save-file-name-transforms
      `((".*" ,emacs-autosave-directory t)))

;;LATEX
(latex-preview-pane-enable)

;; defining a function that sets more accessible keyboard-bindings to
;; hiding/showing code-blocs
(defun hideshow-on()
  (local-set-key (kbd "C-c <right>") 'hs-show-block)
  (local-set-key (kbd "C-c <left>")  'hs-hide-block)
  (local-set-key (kbd "C-c <up>")    'hs-hide-all)
  (local-set-key (kbd "C-c <down>")  'hs-show-all)
  (hs-minor-mode t))

;; activate hideshow for these modes
(add-hook 'emacs-lisp-mode-hook 'hideshow-on)
(add-hook 'python-mode-hook 'hideshow-on)


;; defining a function that sets the right indentation to the marked
;; text, or the entire buffer if no text is selected.
(defun tidy ()
  "Ident, untabify and unwhitespacify current buffer, or region if active."
  (interactive)
  (let ((beg (if (region-active-p) (region-beginning) (point-min)))
        (end (if (region-active-p) (region-end)       (point-max))))
    (whitespace-cleanup)
    (indent-region beg end nil)
    (untabify beg end)))

;; bindes the tidy-function to C-TAB
(global-set-key (kbd "<C-tab>") 'tidy)

;; change active window with M-arrows
(global-set-key (kbd "<M-up>") 'windmove-up)
(global-set-key (kbd "<M-down>") 'windmove-down)
(global-set-key (kbd "<M-left>") 'windmove-left)
(global-set-key (kbd "<M-right>") 'windmove-right)

;; horisontal scroll with touchpad
(global-set-key (kbd "<M-mouse-5>")
		'(lambda () (interactive) (scroll-left 2)))
(global-set-key (kbd "<triple-mouse-7>")
		'(lambda () (interactive) (scroll-left 2)))
(global-set-key (kbd "<M-mouse-4>")
		'(lambda () (interactive) (scroll-right 2)))
(global-set-key (kbd "<triple-mouse-6>")
		'(lambda () (interactive) (scroll-right 2)))
(put 'scroll-left 'disabled nil)
(setq mouse-autoselect-window t)


;;Autosave before exit (No annoying questions)
(defun custon-autosave()
  (interactive)
  (save-buffer)
  (save-buffers-kill-terminal))
(global-set-key (kbd "C-x C-c") 'custon-autosave)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(doc-view-continuous t)
 '(package-selected-packages
   (quote
    (undo-tree multiple-cursors
	       monokai-theme
	       ido-vertical-mode
	       latex-preview-pane
	       auto-complete))))
